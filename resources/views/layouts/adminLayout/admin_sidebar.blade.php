<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="active"><a href="{{url ('/admin/dashboard')}}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Data Master</span> <span class="label label-important">2</span></a>
      <ul>
        <li><a href="{{ url('/admin/user-view') }}">Users</a></li>
        <li><a href="{{ url('/admin/category-view') }}">Category</a></li>
        <li><a href="{{ url('/admin/toko-view') }}">Toko</a></li>
        <li><a href="{{ url('/admin/product-view') }}">Product</a></li>
      </ul>
    </li>
    <li> <a href="{{ url('/admin/order-view') }}"><i class="icon icon-signal"></i> <span>Order</span></a> </li>
    <li> <a href="charts.html"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span></a> </li>
    
    <li class="content"> <span>Disk Space Usage</span>
      <div class="progress progress-mini active progress-striped">
        <div style="width: 87%;" class="bar"></div>
      </div>
      <span class="percent">87%</span>
      <div class="stat">604.44 / 4000 MB</div>
    </li>
  </ul>
</div>
<!--sidebar-menu-->