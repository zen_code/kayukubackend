@extends('layouts.frontLayout.front_design')
@section('content')

<div id="contact" class="section-cotent">
    <div class="container">
        <div class="title-section text-center">
            <h2>Contact Us</h2>
            <span></span>
        </div> <!-- /.title-section -->
        <div class="row">
            <div class="col-md-7 col-sm-6">
                <h4 class="widget-title">Send a message to us</h4>
                <div class="contact-form">
                    <p class="full-row">
                        <label for="name-id">Your Name:</label>
                        <input type="text" id="name-id" name="name-id">
                    </p>
                    <p class="full-row">
                        <label for="email-id">Email:</label>
                        <input type="text" id="email-id" name="email-id">
                    </p>
                    <p class="full-row">
                        <label for="subject-id">Subject:</label>
                        <input type="text" id="subject-id" name="subject-id">
                    </p>
                    <p class="full-row">
                        <label for="message">Message:</label>
                        <textarea name="message" id="message" rows="6"></textarea>
                    </p>
                    <input class="mainBtn" type="submit" name="" value="Send Message">
                </div>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-5 col-sm-6">
                <h4 class="widget-title">Our location</h4>
                <div class="map-holder">
                    <div class="google-map-canvas" id="map-canvas" style="height: 250px;">
                    </div>
                </div> <!-- /.map-holder -->
                <div class="contact-info">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, modi, non ducimus nesciunt in commodi similique aliquam omnis ea at!</p>
                    <span><i class="fa fa-home"></i>850 In luctus justo vel nisi, Duis mattis 10440</span>
                    <span><i class="fa fa-phone"></i>080-080-0990</span>
                    <span><i class="fa fa-envelope"></i>info@company.com</span>
                </div>
            </div> <!-- /.col-md-3 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /#contact -->

@endsection