@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">User</a> <a href="#" class="current">View User</a> </div>
    <h1>User</h1>
    @if(Session::has('flash_message_success'))
        <div class="alert alert-succes alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {!! session ('flash_message_success') !!} </strong>
        </div>
      @endif
      @if(Session::has('flash_message_error'))
        <div class="alert alert-succes alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {!! session ('flash_message_error') !!} </strong>
        </div>
      @endif  
  </div>
  <div class="container-fluid">
    <hr>
    <a href="{{url('/admin/user-add') }}" class="btn btn-success btn-mini">Tambah</a> 
    <div class="row-fluid">
      <div class="span12">
        </div>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View User</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>User ID</th>
                  <th>Nama User</th>
                  <th>Email User</th>
                  <th>Jenis Kelamin</th>
                  <th>Alamat User</th>
                  <th>Image User</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($user as $user)
                <tr class="gradeX">
                  <td>{{ $user->id }}</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->jenis_kelamin }}</td>
                  <td>{{ $user->address }}</td>
                  <td>
                    @if(!empty($user->image))
                    <img src="{{asset ('/images/backend_images/users/small/' .$user->image) }}" style="width: 50px">
                    @endif
                  </td>
                  <td class="center">
                    <a href="#myModal{{ $user->id }}" data-toggle="modal" class="btn btn-success btn-mini">Detail</a> 
                    <a href="{{url('/admin/user-update/'.$user->id) }}" class="btn btn-primary btn-mini">Edit</a>
                    <a href="{{url('/admin/user-delete/'.$user->id) }}" class="btn btn-danger btn-mini">Delete</a>  
                  </td>
                </tr>

                <div id="myModal{{ $user->id }}" class="modal hide">
                  <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>{{ $user->name }} Detail</h3>
                  </div>
                  <div class="modal-body">
                    <p>Nama User = {{ $user->name }}</p>
                    <p>Email User = {{ $user->email }}</p>
                    <p>Jenis_Kelamin= {{ $user->jenis_kelamin }}</p>
                    <p>Alamat User = {{ $user->address }}</p>
                  </div>
                </div>

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection