@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">User</a> <a href="#" class="current">Add User</a> </div>
    <h1>User</h1>
    
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>User</h5>
          </div>
          <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/user-add') }}" name="add_user" id="add_user" novalidate="novalidate"> {{ csrf_field()}}

              <div class="control-group">
                <label class="control-label">Nama User</label>
                <div class="controls">
                  <input type="text" name="name" id="name">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Email User</label>
                <div class="controls">
                  <input type="email" name="email" id="email">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Password</label>
                <div class="controls">
                  <input type="password" name="password" id="password">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Jenis Kelamin</label>
                <div class="controls">
                  <input type="text" name="jenis_kelamin" id="telp">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Address User</label>
                <div class="controls">
                  <input type="text" name="address" id="address">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">User Image</label>
                <div class="controls">
                  <input type="file" name="image" id="image">
                </div>
              </div>

              <div class="form-actions">
                <input type="submit" value="Add User" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  
  </div>
</div>

@endsection
