@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Visitors</a> <a href="#" class="current">Edit Visitor</a> </div>
    <h1>Visitors</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Visitors</h5>
          </div>
          <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/user-update/'.$userDetail->id) }}"   name="edit_user" id="edit_user" novalidate="novalidate"> {{ csrf_field()}}
              <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                  <input type="text" name="name" id="name" value="{{ $userDetail->name }}">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Username</label>
                <div class="controls">
                  <input type="text" name="username" id="username" value="{{ $userDetail->username }}">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                  <input type="email" name="email" id="email" value="{{ $userDetail->email }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Password</label>
                <div class="controls">
                  <input type="password" name="password" id="password" value="{{ $userDetail->password }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                  <input type="text" name="address" id="address" value="{{ $userDetail->address }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Image</label>
                <div class="controls">

                  <input type="file" name="image" id="image">
                  <input type="hidden" name="current_image" value="{{ $userDetail->image }}">
                  @if(!empty($userDetail->image))
                    <img style="width: 30px; " src="{{asset ('/images/backend_images/visitors/small/' .$userDetail->image) }}"> | <a href="{{ url('/admin/delete-visitor-image/'.$userDetail->id) }}">Delete</a>
                  @endif
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Edit Visitor" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  
  </div>
</div>

@endsection