@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Product</a> <a href="#" class="current">View Product</a> </div>
    <h1>Product</h1>
    @if(Session::has('flash_message_success'))
        <div class="alert alert-succes alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {!! session ('flash_message_success') !!} </strong>
        </div>
      @endif
      @if(Session::has('flash_message_error'))
        <div class="alert alert-succes alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {!! session ('flash_message_error') !!} </strong>
        </div>
      @endif  
  </div>
  <div class="container-fluid">
    <hr>
    <a href="{{url('/admin/product-add') }}" class="btn btn-success btn-mini">Tambah</a> 
    <div class="row-fluid">
      <div class="span12">
        </div>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Product</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Product ID</th>
                  <th>Nama Product</th>
                  <th>Category Product</th>
                  <th>Deskripsi</th>
                  <th>Harga</th>
                  <th>Stok</th>
                  <th>Berat</th>
                  <th>Pemilik Toko</th>
                  <th>Product Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($product as $product)
                <tr class="gradeX">
                  <td>{{ $product->id }}</td>
                  <td>{{ $product->name }}</td>
                  <td>{{ $product->category_id }}</td>
                  <td>{{ $product->deskripsi }}</td>
                  <td>{{ $product->harga }}</td>
                  <td>{{ $product->stok }}</td>
                  <td>{{ $product->berat }}</td>
                  <td>{{ $product->toko_id }}</td>
                  <td>
                    @if(!empty($product->image))
                    <img src="{{asset ('/images/backend_images/products/small/' .$product->image) }}" style="width: 50px">
                    @endif
                  </td>
                  <td class="center">
                    <a href="#myModal{{ $product->id }}" data-toggle="modal" class="btn btn-success btn-mini">Detail</a> 
                    <a href="{{url('/admin/product-update/'.$product->id) }}" class="btn btn-primary btn-mini">Edit</a>
                    <a href="{{url('/admin/product-delete/'.$product->id) }}" class="btn btn-danger btn-mini">Delete</a>  
                  </td>
                </tr>

                <div id="myModal{{ $product->id }}" class="modal hide">
                  <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>{{ $product->name }} Detail</h3>
                  </div>
                  <div class="modal-body">
                    <p>Nama Product = {{ $product->name }}</p>
                    <p>Category Product = {{ $product->category_id }}</p>
                    <p>Deskripsi Product= {{ $product->deskripsi }}</p>
                    <p>Harga Product = {{ $product->harga }}</p>
                    <p>Stok Product = {{ $product->stok }}</p>
                    <p>Berat Product = {{ $product->berat }}</p>
                    <p>Pemilik Toko = {{ $product->toko_id }}</p>
                  </div>
                </div>

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection