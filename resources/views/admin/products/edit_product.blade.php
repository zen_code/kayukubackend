@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Visitors</a> <a href="#" class="current">Edit Visitor</a> </div>
    <h1>Visitors</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Visitors</h5>
          </div>
          <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/visitor-update/'.$visitorDetail->id) }}"   name="edit_visitor" id="edit_visitor" novalidate="novalidate"> {{ csrf_field()}}
              <div class="control-group">
                <label class="control-label">Visitor Name</label>
                <div class="controls">
                  <input type="text" name="name" id="name" value="{{ $visitorDetail->name }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Visitor Email</label>
                <div class="controls">
                  <input type="email" name="email" id="email" value="{{ $visitorDetail->email }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Visitor Password</label>
                <div class="controls">
                  <input type="password" name="password" id="password" value="{{ $visitorDetail->password }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Visitor Category</label>
                <div class="controls">
                  <input type="text" name="category_id" id="category_id" value="{{ $visitorDetail->category_id }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Visitor Telp</label>
                <div class="controls">
                  <input type="text" name="telp" id="telp" value="{{ $visitorDetail->telp }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Visitor Address</label>
                <div class="controls">
                  <input type="text" name="address" id="address" value="{{ $visitorDetail->address }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Visitor Image</label>
                <div class="controls">

                  <input type="file" name="image" id="image">
                  <input type="hidden" name="current_image" value="{{ $visitorDetail->image }}">
                  @if(!empty($visitorDetail->image))
                    <img style="width: 30px; " src="{{asset ('/images/backend_images/visitors/small/' .$visitorDetail->image) }}"> | <a href="{{ url('/admin/delete-visitor-image/'.$visitorDetail->id) }}">Delete</a>
                  @endif
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Edit Visitor" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  
  </div>
</div>

@endsection