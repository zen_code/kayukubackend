@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Product</a> <a href="#" class="current">Add Product</a> </div>
    <h1>Product</h1>
    
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Product</h5>
          </div>
          <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/product-add') }}" name="add_product" id="add_product" novalidate="novalidate"> {{ csrf_field()}}
              <label class="control-label">Pemilik Toko</label>
              <div class="controls">
                <select name="toko_id" style="width:220px;">
                  @foreach($tokoOption as $toko)
                  <option value="{{ $toko->id }}">{{ $toko->name }}</option>
                  @endforeach
                </select>
              </div>

              <label class="control-label">Category Product</label>
              <div class="controls">
                <select name="category_id" style="width:220px;">
                  @foreach($categoryOption as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>
              </div>

              <div class="control-group">
                <label class="control-label">Nama Product</label>
                <div class="controls">
                  <input type="text" name="name" id="name">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Deskripsi Product</label>
                <div class="controls">
                  <textarea type="text" name="deskripsi" id="deskripsi"> </textarea>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Berat Product</label>
                <div class="controls">
                  <input type="text" name="berat" id="berat">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Harga Product</label>
                <div class="controls">
                  <input type="text" name="harga" id="harga">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Stok Product</label>
                <div class="controls">
                  <input type="text" name="stok" id="stok">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Product Image</label>
                <div class="controls">
                  <input type="file" name="image" id="image">
                </div>
              </div>

              <div class="form-actions">
                <input type="submit" value="Add Product" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  
  </div>
</div>

@endsection
