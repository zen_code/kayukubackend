@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Toko</a> <a href="#" class="current">Add Toko</a> </div>
    <h1>Toko</h1>
    
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Toko</h5>
          </div>
          <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/toko-add') }}" name="add_toko" id="add_toko" novalidate="novalidate"> {{ csrf_field()}}
              <label class="control-label">Pemilik Toko</label>
              <div class="controls">
                <select name="user_id" style="width:220px;">
                  @foreach($userOption as $user)
                  <option value="{{ $user->id }}">{{ $user->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="control-group">
                <label class="control-label">Nama Toko</label>
                <div class="controls">
                  <input type="text" name="name" id="name">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Alamat Toko</label>
                <div class="controls">
                  <input type="text" name="alamat" id="alamat">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Telp Toko</label>
                <div class="controls">
                  <input type="text" name="telp" id="telp">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Informasi Toko</label>
                <div class="controls">
                  <input type="text" name="informasi" id="informasi">
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Add Toko" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  
  </div>
</div>

@endsection
