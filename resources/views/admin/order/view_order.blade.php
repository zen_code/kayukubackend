@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Order</a> <a href="#" class="current">View Order</a> </div>
    <h1>Order</h1>
    @if(Session::has('flash_message_success'))
        <div class="alert alert-succes alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {!! session ('flash_message_success') !!} </strong>
        </div>
      @endif
      @if(Session::has('flash_message_error'))
        <div class="alert alert-succes alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {!! session ('flash_message_error') !!} </strong>
        </div>
      @endif  
  </div>
  <div class="container-fluid">
    <hr>
    <a href="{{url('/admin/order-add') }}" class="btn btn-success btn-mini">Tambah</a> 
    <div class="row-fluid">
      <div class="span12">
        </div>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Toko</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Customers ID</th>
                  <th>Customers Nama</th>
                  <th>Toko ID</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Harga</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($order as $order)
                <tr class="gradeX">
                  <td>{{ $order->customers_id }}</td>
                  <td>{{ $order->customers_name }}</td>
                  <td>{{ $order->toko_id }}</td>
                  <td>{{ $order->name }}</td>
                  <td>{{ $order->image }}</td>
                  <td>{{ $order->harga }}</td>
                  <td>{{ $order->status}}</td>
                  <td class="center">
                    <a href="#myModal{{ $toko->id }}" data-toggle="modal" class="btn btn-success btn-mini">Detail</a> 
                    <a href="{{url('/admin/toko-update/'.$toko->id) }}" class="btn btn-primary btn-mini">Edit</a>
                    <a href="{{url('/admin/toko-delete/'.$toko->id) }}" class="btn btn-danger btn-mini">Delete</a>  
                  </td>
                </tr>

                <div id="myModal{{ $toko->id }}" class="modal hide">
                  <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>{{ $toko->name }} Detail</h3>
                  </div>
                  <div class="modal-body">
                    <p>Pemilik Toko = {{ $toko->user_id }}</p>
                    <p>Nama Toko = {{ $toko->name }}</p>
                    <p>Alamat = {{ $toko->alamat }}</p>
                    <p>Telp = {{ $toko->telp }}</p>
                    <p>Informasi Toko = {{ $toko->informasi }}</p>
                  </div>
                </div>

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection