
$(document).ready(function(){

	$("#new_pwd").click(function() {
		var current_pwd = $("#current_pwd").val();
		$.ajax({
			type:'get',
			url:'check-pwd',
			data:{current_pwd:current_pwd},
			success:function(resp) {
				if (resp=='true') {
					$("#chkPwd").html("<font-color='red'>Current password is Correct </font>");
				} else if (resp=='false') {
					$("#chkPwd").html("<font-color='green'>Current password is Incorrect </font>");
				}
			}, error:function() {
				alert("Error");
			}
		}) 
	});
	
	$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
	
	$('select').select2();
	
	// Form Validation
    $("#add_category").validate({
		rules:{
			name:{
				required:true,
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	$("#edit_category").validate({
		rules:{
			name:{
				required:true,
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	$("#add_visitor").validate({
		rules:{
			name:{
				required:true,
			},
			email:{
				required:true,
			},
			password:{
				required:true,
			},
			category_id:{
				required:true,
			},
			telp:{
				required:true,
			},
			address:{
				required:true,
			},
			image:{
				required:true,
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});
	
	$("#number_validate").validate({
		rules:{
			min:{
				required: true,
				min:10
			},
			max:{
				required:true,
				max:24
			},
			number:{
				required:true,
				number:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});
	
	$("#password_validate").validate({
		rules:{
			current_pwd:{
				required: true,
				minlength:6,
				maxlength:20
			},
			new_pwd:{
				required: true,
				minlength:6,
				maxlength:20
			},
			confirm_pwd:{
				required:true,
				minlength:6,
				maxlength:20,
				equalTo:"#new_pwd"
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	$("delVisitor").click(function()) {
		if (confirm("Are you sure to delete this visitor ?")) {
			return true;
		}
		return false;
	}

	// $(document).on('click', .'deleteVisitor', function(e)) {
	// 	var id = $(this).attr('rel');
	// 	alert(id);
	// 	return false;
	// }
});
