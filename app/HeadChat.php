<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadChat extends Model
{
    protected $table = 'header_chat';
}
