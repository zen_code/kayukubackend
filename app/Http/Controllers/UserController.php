<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use Image;

class UserController extends Controller
{
    public function add(Request $request) {
    	if ($request->isMethod('post')) {
    		$data = $request->all();
			// echo "<pre>"; print_r($data); die;
			//dd($data['parent_id']);
    		$user = new User;
			$user->name = $data['name'];
			$user->email = $data['email'];
			$user->password = sha1($data['password']);
            $user->jenis_kelamin = $data['jenis_kelamin'];
			$user->address = $data['address'];

			//UploadFile
            if ($request->hasFile('image')) {
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111, 99999).'.'.$extension;
                    $large_image_path = 'images/backend_images/users/large/' .$filename;
                    $medium_image_path = 'images/backend_images/users/medium/' .$filename;
                    $small_image_path = 'images/backend_images/users/small/' .$filename;

                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300, 300)->save($small_image_path);

                    $user->image = $filename;

                }
            }

    		$user->save();
    		return redirect('/admin/user-view')->with('flash_message_success', 'User added successfully');
		}

    	return view('admin.users.add_user');
    }

    public function getUser() {
    	// echo "test";
    	$user = User::get();
    	$user = json_decode(json_encode($user));
    	// echo "<pre>"; print_r($categories); die;
    	return view('admin.users.view_user')->with(compact('user'));
    }

    public function updateUser(Request $request, $id = null) {
    	// echo "string";
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;

    		if ($request->hasFile('image')) {
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111, 99999).'.'.$extension;
                    $large_image_path = 'images/backend_images/users/large/' .$filename;
                    $medium_image_path = 'images/backend_images/users/medium/' .$filename;
                    $small_image_path = 'images/backend_images/users/small/' .$filename;

                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300, 300)->save($small_image_path);

                }
            } else {
                $filename = $data['current_image'];

            }

    		User::where(['id' => $id])->update([
				'user_id' => $data['user_id'],
				'name' => $data['name'],
				'email' => $data['email'],
				'telp' => $data['telp'],
				'address' => $data['address'],
				'image' => $filename

    		]);
    		return redirect('/admin/users-view')->with('flash_message_success', 'User edited successfully');
    	}
		$userDetail = User::where(['id' => $id])->first();

    	return view('admin.users.edit_user')->with(compact('userDetail', 'levels'));
    }

    public function deleteUser(Request $request, $id = null) {
    	if (!empty($id)) {
    		User::where(['id' => $id])->delete();
    		return redirect()->back()->with('flash_message_success', 'User was deleted successfully');
    	}
    }
}
