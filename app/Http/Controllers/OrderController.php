<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderList;
use App\Product;
use App\User;

class OrderController extends Controller
{
    public function add(Request $request) {
    	if ($request->isMethod('post')) {
    		$data = $request->all();
			// echo "<pre>"; print_r($data); die;
			//dd($data['parent_id']);
    		$order = new OrderList;
			$order->customers_id = $data['customers_id'];
			$order->customers_name = $data['customers_name'];
			$order->toko_id = $data['toko_id'];
            $order->name = $data['name'];
			$order->image = $data['image'];
			$order->harga = $data['harga'];
			$order->status = $data['status'];
			
            //UploadFile
            if ($request->hasFile('image')) {
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111, 99999).'.'.$extension;
                    $large_image_path = 'images/products/large/' .$filename;
                    $medium_image_path = 'images/products/medium/' .$filename;
                    $small_image_path = 'images/products/small/' .$filename;

                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300, 300)->save($small_image_path);

                    $order->image = $filename;

                }
            }

    		$order->save();
    		return redirect('/admin/order-view')->with('flash_message_success', 'Product added successfully');
		}

		$params=[
            'tokoOption'=>Toko::all(),
            'categoryOption' => Category::all()
        ];
		
    	return view('admin.products.add_product', $params);
    }

    public function getOrder() {
    	// echo "test";
    	$order = Product::get();
    	$order = json_decode(json_encode($order));
    	// echo "<pre>"; print_r($categories); die;
    	return view('admin.order.view_order')->with(compact('order'));
    }

    public function updateProduct(Request $request, $id = null) {
    	// echo "string";
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;

            if ($request->hasFile('image')) {
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111, 99999).'.'.$extension;
                    $large_image_path = 'images/backend_images/products/large/' .$filename;
                    $medium_image_path = 'images/backend_images/products/medium/' .$filename;
                    $small_image_path = 'images/backend_images/products/small/' .$filename;

                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300, 300)->save($small_image_path);

                }
            } else {
                $filename = $data['current_image'];

            }

    		Product::where(['id' => $id])->update([
				'toko_id' => $data['toko_id'],
				'category_id' => $data['category_id'],
				'name' => $data['name'],
				'deskripsi' => $data['deskripsi'],
				'berat' => $data['berat'],
				'harga' => $data['harga'],
				'stok' => $data['stok'],
				'image' => $filename
               
    		]);
    		return redirect('/admin/product-view')->with('flash_message_success', 'Product edited successfully');
    	}
		$productDetail = Product::where(['id' => $id])->first();

    	return view('admin.products.edit_product')->with(compact('productDetail', 'levels'));
    }

    public function deleteProduct(Request $request, $id = null) {
    	if (!empty($id)) {
    		Product::where(['id' => $id])->delete();
    		return redirect()->back()->with('flash_message_success', 'Product was deleted successfully');
    	}
    }
}
