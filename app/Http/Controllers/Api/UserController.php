<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $apiName='REGISTRASI';
        $name=$request->name;
        $email=$request->email;
        $jenis_kelamin=$request->jenis_kelamin;
        $username=$request->username;
        $password=sha1($request->password);

        $sendingParams = [
            'name' => $name,
            'email' => $email,
            'jenis_kelamin' => $jenis_kelamin,
            'username' => $username,
            'password' => $password,
        ];

        try {
            $data = new User();
            $data->name=$name;
            $data->email=$email;
            $data->jenis_kelamin=$jenis_kelamin;
            $data->username=$username;
            $data->password=$password;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function updateProfile(Request $request)
    {
        $apiName='UPDATEPROFILE';
        $id=$request->id;
        $name=$request->name;
        $email=$request->email;
        $address=$request->address;
        $filename=null;
        $image = $request->file('image');
        if($image){
            $destinationPath = 'images/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        } else {
            $filename=null;
        }

        $sendingParams = [
            'id' => $id,
            'name' => $name,
            'email' => $email,
            'address' => $address,
            'image' => $image,
        ];

        try {
            $data = User::find($id);
            $data->name=$name;
            $data->email=$email;
            $data->address=$address;
            $data->image=$filename;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }


    public function login(Request $request)
    {
        $username = $request->username;
        $password = sha1($request->password);

        $activeUser = User::where('username',$username)->first();
        if ($activeUser!=null) {
            if ($activeUser->password === $password) {
                $activeUser->save();

                $sendingParams = [

                    'is_success' => true,
                    'status' => 200,
                    'message' => 'success',
                    'data' => $activeUser
                ];

                return response()-> json($sendingParams);
            }
        }

        if(is_null($activeUser)){
           $sendingParams = [
               'is_success' => false,
               'status' => 404,
               'message' => 'username tidak ada',
            ];
            return response()-> json($sendingParams);
        }

       if ($activeUser -> password != $password) {
            $sendingParams = [
                'is_success' => false,
                'status' => 401,
                'message' => 'password salah',
            ];
            return response()-> json($sendingParams);
        }
    }

    public function validasiUser(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        $activeUser = User::where('username',$username)->first();
        if ($activeUser!=null) {
            if ($activeUser->password === $password) {
                $activeUser->save();

                $sendingParams = [

                    'is_success' => true,
                    'status' => 200,
                    'message' => 'success',
                    'data' => $activeUser
                ];

                return response()-> json($sendingParams);
            }
        }

        if(is_null($activeUser)){
           $sendingParams = [
               'is_success' => false,
               'status' => 404,
               'message' => 'username tidak ada',
            ];
            return response()-> json($sendingParams);
        }

       if ($activeUser -> password != $password) {
            $sendingParams = [
                'is_success' => false,
                'status' => 401,
                'message' => 'password salah',
            ];
            return response()-> json($sendingParams);
        }
    }

    public function getUser(Request $request)
    {
        return response()->json([
            "status"=>"berhasil",
            "code"=>"200",
            "data"=>User::all(),
        ]);
    }
}
