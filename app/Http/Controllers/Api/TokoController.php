<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Toko;

class TokoController extends Controller
{
    public function registertoko(Request $request)
    {
        $apiName='REGISTRASI';
        $user_id=$request->user_id;
        $name=$request->name;
        $sendingParams = [
            'user_id' => $user_id,
            'name' => $name,
        ];

        try {
            $data = new Toko();
            $data->user_id=$user_id;
            $data->name=$name;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function updateToko(Request $request)
    {
        $apiName='UPDATEPROFILETOKO';
        $id=$request->id;
        $user_id=$request->user_id;
        $name=$request->name;
        $alamat=$request->alamat;
        $telp=$request->telp;
        $informasi=$request->informasi;

        $filename=null;
        $image = $request->file('image');
        if($image){
            $destinationPath = 'images/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        } else {
            $filename=null;
        }

        $sendingParams = [
            'id' => $id,
            'user_id' => $user_id,
            'name' => $name,
            'alamat' => $alamat,
            'telp' => $telp,
            'informasi' => $informasi,
            'image' => $image,
        ];

        try {
            $data = Toko::find($id);
            $data->user_id=$user_id;
            $data->name=$name;
            $data->alamat=$alamat;
            $data->telp=$telp;
            $data->informasi=$informasi;
            $data->image=$filename;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function getToko(Request $request)
    {
        return response()->json([
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            "data"=>Toko::all(),
        ]);
    }

    public function getTokoUser(Request $request)
    {
      $apiName='GETTOKOUSER';
      $id=$request->id;

      $sendingParams = [
          'id' => $id,
      ];

      try {
          $data = Toko::find($id);

          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

          return response()->json($params);

      } catch (Exception $e) {

          return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
      }

      return response()->json($params);
    }
}
