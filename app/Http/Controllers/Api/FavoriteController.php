<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Favorite;

class FavoriteController extends Controller
{
    public function addFavorite(Request $request)
    {
        $apiName='FAVORITE';
        $id_produk=$request->id_produk;
        $customers_id=$request->customers_id;
        $toko_id=$request->toko_id;
        $name=$request->name;
        $deskripsi=$request->deskripsi;
        $harga=$request->harga;
        $image_primary=$request->image_primary;

        $sendingParams = [
            'id_produk' => $id_produk,
            'customers_id' => $customers_id,
            'toko_id' => $toko_id,
            'name' => $name,
            'deskripsi' => $deskripsi,
            'harga' => $harga,
            'image_primary' => $image_primary,
        ];

        try {
            $data = new Favorite();
            $data->id_produk=$id_produk;
            $data->customers_id=$customers_id;
            $data->toko_id=$toko_id;
            $data->name=$name;
            $data->deskripsi=$deskripsi;
            $data->harga=$harga;
            $data->image_primary=$image_primary;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }


    public function getFavorite(Request $request)
    {
        return response()->json([
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            "data"=>Favorite::all(),
        ]);
    }

    public  function deleteFavorite(Request $request){

        $apiName='DELETEFAVORITE';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            Favorite::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
