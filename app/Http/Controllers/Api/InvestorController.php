<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Investor;

class InvestorController extends Controller
{
    public function addInvestor(Request $request)
    {
        $apinama='ADDINVESTOR';
        $nama=$request->nama;
        $deskripsi=$request->deskripsi;
        $alamat=$request->alamat;
        $tlp=$request->tlp;
        $email=$request->email;
        $pinjaman=$request->pinjaman;
        $filename=null;
        $image = $request->file('image');
        if($image){
            $destinationPath = 'images/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        } else {
            $filename=null;
        }

        $sendingParams = [
            'nama' => $nama,
            'deskripsi' => $deskripsi,
            'alamat' => $alamat,
            'tlp' => $tlp,
            'email' => $email,
            'pinjaman' => $pinjaman,
            'image' => $image,
        ];

        try {
            $data = new Investor();
            $data->nama=$nama;
            $data->deskripsi=$deskripsi;
            $data->alamat=$alamat;
            $data->tlp=$tlp;
            $data->email=$email;
            $data->pinjaman=$pinjaman;
            $data->image=$filename;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apinama,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function getInvestor(Request $request)
    {
        return response()->json([
            "status"=>"berhasil",
            "code"=>"200",
            "data"=>Investor::all(),
        ]);
    }

    public  function deleteInvestor(Request $request){

        $apinama='DELEINVESTOR';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            Investor::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apinama,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
