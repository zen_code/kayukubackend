<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ulasan;

class UlasanController extends Controller
{
    public function addUlasan(Request $request)
    {
        $apiName='ADDULASAN';
        $produk_id=$request->produk_id;
        $customers_id=$request->customers_id;
        $customers_name=$request->customers_name;
        $toko_id=$request->toko_id;
        $date=$request->date;
        $ulasan=$request->ulasan;
        $image=$request->image;

        $sendingParams = [
            'produk_id' => $produk_id,
            'customers_id' => $customers_id,
            'customers_name' => $customers_name,
            'toko_id' => $toko_id,
            'date' => $date,
            'ulasan' => $ulasan,
            'image' => $image,
        ];

        try {
            $data = new Ulasan();
            $data->produk_id=$produk_id;
            $data->customers_id=$customers_id;
            $data->customers_name=$customers_name;
            $data->toko_id=$toko_id;
            $data->date=$date;
            $data->ulasan=$ulasan;
            $data->image=$image;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }


    public function getUlasan(Request $request)
    {
        return response()->json([
            "status"=>"berhasil",
            "code"=>"200",
            "data"=>Ulasan::all(),
        ]);
    }

    public  function deleteUlasan(Request $request){

        $apiName='DELETEULASAN';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            Ulasan::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
