<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    public function getCategory(Request $request)
    {
        return response()->json([
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            "data"=>Category::all(),
        ]);
    }
}
