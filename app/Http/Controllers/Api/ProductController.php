<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
    public function addProduct(Request $request)
    {
        $apiName='ADDPRODUK';
        $toko_id=$request->toko_id;
        $category_id=$request->category_id;
        $name=$request->name;
        $deskripsi=$request->deskripsi;
        $berat=$request->berat;
        $harga=$request->harga;
        $stok=$request->stok;

        $filename_primary=null;
        $image_primary = $request->file('image_primary');
        if($image_primary){
            $destinationPath = 'images/';
            $filename_primary = $image_primary->getClientOriginalName();
            $image_primary->move($destinationPath, $filename_primary);
        } else {
            $filename_primary=null;
        }
        $filename_secondary=null;
        $image_secondary = $request->file('image_secondary');
        if($image_secondary){
            $destinationPath = 'images/';
            $filename_secondary = $image_secondary->getClientOriginalName();
            $image_secondary->move($destinationPath, $filename_secondary);
        } else {
            $filename_secondary=null;
        }

        $sendingParams = [
            'toko_id' => $toko_id,
            'category_id' => $category_id,
            'name' => $name,
            'deskripsi' => $deskripsi,
            'berat' => $berat,
            'harga' => $harga,
            'stok' => $stok,
            'image_primary' => $image_primary,
            'image_secondary' => $image_secondary,
        ];

        try {
            $data = new Product();
            $data->toko_id=$toko_id;
            $data->category_id=$category_id;
            $data->name=$name;
            $data->deskripsi=$deskripsi;
            $data->berat=$berat;
            $data->harga=$harga;
            $data->stok=$stok;
            $data->image_primary=$filename_primary;
            $data->image_secondary=$filename_secondary;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public function updateProduct(Request $request)
    {
        $apiName='UPDATEPRODUK';
        $id=$request->id;
        $toko_id=$request->toko_id;
        $category_id=$request->category_id;
        $name=$request->name;
        $deskripsi=$request->deskripsi;
        $berat=$request->berat;
        $harga=$request->harga;
        $stok=$request->stok;
        $filename_primary=null;
        $image_primary = $request->file('image_primary');
        if($image_primary){
            $destinationPath = 'images/';
            $filename_primary = $image_primary->getClientOriginalName();
            $image_primary->move($destinationPath, $filename_primary);
        } else {
            $filename_primary=null;
        }
        $filename_secondary=null;
        $image_secondary = $request->file('image_secondary');
        if($image_secondary){
            $destinationPath = 'images/';
            $filename_secondary = $image_secondary->getClientOriginalName();
            $image_secondary->move($destinationPath, $filename_secondary);
        } else {
            $filename_secondary=null;
        }

        $sendingParams = [
            'toko_id' => $toko_id,
            'category_id' => $category_id,
            'name' => $name,
            'deskripsi' => $deskripsi,
            'berat' => $berat,
            'harga' => $harga,
            'stok' => $stok,
            'image_primary' => $image_primary,
            'image_secondary' => $image_secondary,
        ];

        try {
            $data = Product::find($id);
            $data->toko_id=$toko_id;
            $data->category_id=$category_id;
            $data->name=$name;
            $data->deskripsi=$deskripsi;
            $data->berat=$berat;
            $data->harga=$harga;
            $data->stok=$stok;
            $data->image_primary=$filename_primary;
            $data->image_secondary=$filename_secondary;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public  function  deleteProduct(Request $request){

        $apiName='DELETEPRODUCT';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            Product::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }

    public function getProduct(Request $request)
    {
        return response()->json([
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            "data"=>Product::all(),
        ]);
    }
}
