<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrderList;

class OrderController extends Controller
{
    public function addOder(Request $request)
    {
        $apiName='ORDERCONTROLLER';
        $customers_id=$request->customers_id;
        $customers_name=$request->customers_name;
        $toko_id=$request->toko_id;
        $name=$request->name;
        $image=$request->image;
        $harga=$request->harga;
        $status=$request->status;

        $sendingParams = [
            'customers_id' => $customers_id,
            'customers_name' => $customers_name,
            'toko_id' => $toko_id,
            'name' => $name,
            'image' => $image,
            'harga' => $harga,
            'status' => $status,
        ];

        try {
            $data = new OrderList();
            $data->customers_id=$customers_id;
            $data->customers_name=$customers_name;
            $data->toko_id=$toko_id;
            $data->name=$name;
            $data->image=$image;
            $data->harga=$harga;
            $data->status=$status;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }


    public function updateOrder(Request $request)
    {
        $apiName='UPDATEORDERCONTROLLER';
        $id=$request->id;
        $customers_id=$request->customers_id;
        $customers_name=$request->customers_name;
        $toko_id=$request->toko_id;
        $kode=$request->kode;
        $name=$request->name;
        $image=$request->image;
        $harga=$request->harga;
        $status=$request->status;
        $filename=null;
        $status_image = $request->file('status_image');
        if($status_image){
            $destinationPath = 'images/';
            $filename = $status_image->getClientOriginalName();
            $status_image->move($destinationPath, $filename);
        } else {
            $filename=null;
        }
        $qty=$request->qty;
        $total=$request->total;
        $alamat=$request->alamat;
        $sendingParams = [
            'id' => $id,
            'customers_id' => $customers_id,
            'customers_name' => $customers_name,
            'toko_id' => $toko_id,
            'kode' => $kode,
            'name' => $name,
            'image' => $image,
            'harga' => $harga,
            'status' => $status,
            'status_image' => $status_image,
            'qty' => $qty,
            'total' => $total,
            'alamat' => $alamat,

        ];

        try {
            $data = OrderList::find($id);
            $data->customers_id=$customers_id;
            $data->customers_name=$customers_name;
            $data->toko_id=$toko_id;
            $data->kode=$kode;
            $data->name=$name;
            $data->image=$image;
            $data->harga=$harga;
            $data->status=$status;
            $data->status_image=$filename;
            $data->qty=$qty;
            $data->total=$total;
            $data->alamat=$alamat;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function getOrder(Request $request)
    {
        return response()->json([
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            "data"=>OrderList::all(),
        ]);
    }

    public  function  deleteOrder(Request $request){

        $apiName='DELETEORDER';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            OrderList::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
