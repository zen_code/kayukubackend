<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Chat;

class ChatController extends Controller
{
    public function addChat(Request $request)
    {
        $apiName='CHAT';
        $id_header=$request->id_header;
        $customers_id=$request->customers_id;
        $message=$request->message;
        $date=$request->date;

        $sendingParams = [
            'id_header' => $id_header,
            'customers_id' => $customers_id,
            'message' => $message,
            'date' => $date
        ];

        try {
            $data = new Chat();
            $data->id_header=$id_header;
            $data->customers_id=$customers_id;
            $data->message=$message;
            $data->date=$date;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }


    public function getChat(Request $request)
    {
        return response()->json([
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            "data"=>Chat::all(),
        ]);
    }
}
