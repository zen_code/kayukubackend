<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HeadChat;

class HeadChatController extends Controller
{
    public function addHeadChat(Request $request)
    {
        $apiName='HEAD_CHAT';
        $customers_id_sender=$request->customers_id_sender;
        $customers_name_sender=$request->customers_name_sender;
        $customers_id_receiver=$request->customers_id_receiver;
        $customers_name_receiver=$request->customers_name_receiver;

        $sendingParams = [
            'customers_id_sender' => $customers_id_sender,
            'customers_name_sender' => $customers_name_sender,
            'customers_id_receiver' => $customers_id_receiver,
            'customers_name_receiver' => $customers_name_receiver,
        ];

        try {
            $data = new HeadChat();
            $data->customers_id_sender=$customers_id_sender;
            $data->customers_name_sender=$customers_name_sender;
            $data->customers_id_receiver=$customers_id_receiver;
            $data->customers_name_receiver=$customers_name_receiver;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }


    public function getHeadChat(Request $request)
    {
        return response()->json([
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            "data"=>HeadChat::all(),
        ]);
    }
}
