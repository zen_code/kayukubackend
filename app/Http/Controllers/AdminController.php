<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use Illuminate\Support\Facades\Hash; 

class AdminController extends Controller
{
    public function login (Request $request) {
    	if ($request->isMethod('post')) {
    		$data = $request->input();
    		if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'admin' => '1'])) {
                // echo "succes"; die;
                //using session biasa setting di controller ini saja
                // Session::put('adminSession', $data['email']);
    			return redirect('/admin/dashboard');
    		} else {
                // echo "failed";
    			return redirect('/admin')->with('flash_message_error', 'Invalid email or password');

    		}
    	}
    	return view('admin.admin_login');
    }

    public function dashboard() {
        //pengkondisian session
        // if (Session::has('adminSession')) {
        
        // } else {
        //     return redirect('/admin')->with('flash_message_error', 'Please Login to access');
        // }
        return view('admin.dashboard');
    }

    public function settings() {
        return view('admin.settings');
    }

    public function chkPassword (Request $request) {
        $data = $request->all();
        $current_password = $data['current_pwd'];
        $checkPassword = User::where(['admin' => '1'])->first();
        if (Hash::check($current_password, $checkPassword->password)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function updatePassword (Request $request) {
        if ($request->isMethod('post')) {
            $data = $request->all();
            // echo "<pre>"; print_r($data); die;

            $checkPassword = User::where(['email' => Auth::User()->email])->first();
            $current_password = $data['current_pwd'];
            if (Hash::check($current_password, $checkPassword->password)) {
                $password = bcrypt($data['new_pwd']);
                User::where('id', '1')->update(['password' => $password]);
                return redirect('/admin/settings')->with('flash_message_succes', 'Password updated successfully');
            } else {
                return redirect('/admin/settings')->with('flash_message_succes', 'Password updated failed');
            }

        }
        
    }


    public function logout() {
        Session::flush();
        return redirect('/admin')->with('flash_message_succes', 'Loged out Succesfully');
    }


}
