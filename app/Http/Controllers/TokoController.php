<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Toko;
use App\User;

class TokoController extends Controller
{
    public function add(Request $request) {
    	if ($request->isMethod('post')) {
    		$data = $request->all();
			// echo "<pre>"; print_r($data); die;
			//dd($data['parent_id']);
    		$toko = new Toko;
			$toko->user_id = $data['user_id'];
			$toko->name = $data['name'];
			$toko->alamat = $data['alamat'];
            $toko->telp = $data['telp'];
			$toko->informasi = $data['informasi'];

    		$toko->save();
    		return redirect('/admin/toko-view')->with('flash_message_success', 'Toko added successfully');
		}

		$params=[
            'userOption'=>User::all()
        ];
		
    	return view('admin.tokos.add_toko', $params);
    }

    public function getToko() {
    	// echo "test";
    	$toko = Toko::get();
    	$toko = json_decode(json_encode($toko));
    	// echo "<pre>"; print_r($categories); die;
    	return view('admin.tokos.view_toko')->with(compact('toko'));
    }

    public function updateToko(Request $request, $id = null) {
    	// echo "string";
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;

    		Toko::where(['id' => $id])->update([
				'user_id' => $data['user_id'],
				'name' => $data['name'],
				'alamat' => $data['alamat'],
				'informasi' => $data['informasi'],
               
    		]);
    		return redirect('/admin/toko-view')->with('flash_message_success', 'Toko edited successfully');
    	}
		$tokoDetail = Toko::where(['id' => $id])->first();

    	return view('admin.tokos.edit_toko')->with(compact('tokoDetail', 'levels'));
    }

    public function deleteToko(Request $request, $id = null) {
    	if (!empty($id)) {
    		Toko::where(['id' => $id])->delete();
    		return redirect()->back()->with('flash_message_success', 'Toko was deleted successfully');
    	}
    }
}
