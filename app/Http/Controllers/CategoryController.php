<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function add(Request $request) {
    	if ($request->isMethod('post')) {
    		$data = $request->all();
			// echo "<pre>"; print_r($data); die;
			//dd($data['parent_id']);
    		$category = new Category;
			$category->name = $data['name'];
    		$category->save();
    		return redirect('/admin/category-view')->with('flash_message_success', 'Category added successfully');
		}
		
    	return view('admin.categories.add_category');
    }

    public function getCategory() {
    	// echo "test";
    	$categories = Category::get();
    	$categories = json_decode(json_encode($categories));
    	// echo "<pre>"; print_r($categories); die;
    	return view('admin.categories.view_category')->with(compact('categories'));
    }

    public function updateCategory(Request $request, $id = null) {
    	// echo "string";
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;

    		Category::where(['id' => $id])->update([
				'name' => $data['name'],
    		]);
    		return redirect('/admin/category-view')->with('flash_message_success', 'Category edited successfully');
    	}
		$categoryDetail = Category::where(['id' => $id])->first();

    	return view('admin.categories.edit_category')->with(compact('categoryDetail', 'levels'));
    }

    public function deleteCategory(Request $request, $id = null) {
    	if (!empty($id)) {
    		Category::where(['id' => $id])->delete();
    		return redirect()->back()->with('flash_message_success', 'Category was deleted successfully');
    	}
    }
}
