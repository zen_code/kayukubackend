<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Product;
use App\Toko;
use App\Category;
use Image;

class ProductController extends Controller
{
    public function add(Request $request) {
    	if ($request->isMethod('post')) {
    		$data = $request->all();
			// echo "<pre>"; print_r($data); die;
			//dd($data['parent_id']);
    		$product = new Product;
			$product->toko_id = $data['toko_id'];
			$product->category_id = $data['category_id'];
			$product->name = $data['name'];
            $product->deskripsi = $data['deskripsi'];
			$product->berat = $data['berat'];
			$product->harga = $data['harga'];
			$product->stok = $data['stok'];
			
            //UploadFile
            if ($request->hasFile('image')) {
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111, 99999).'.'.$extension;
                    $large_image_path = 'images/backend_images/products/large/' .$filename;
                    $medium_image_path = 'images/backend_images/products/medium/' .$filename;
                    $small_image_path = 'images/backend_images/products/small/' .$filename;

                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300, 300)->save($small_image_path);

                    $product->image = $filename;

                }
            }

    		$product->save();
    		return redirect('/admin/product-view')->with('flash_message_success', 'Product added successfully');
		}

		$params=[
            'tokoOption'=>Toko::all(),
            'categoryOption' => Category::all()
        ];
		
    	return view('admin.products.add_product', $params);
    }

    public function getProduct() {
    	// echo "test";
    	$product = Product::get();
    	$product = json_decode(json_encode($product));
    	// echo "<pre>"; print_r($categories); die;
    	return view('admin.products.view_product')->with(compact('product'));
    }

    public function updateProduct(Request $request, $id = null) {
    	// echo "string";
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;

            if ($request->hasFile('image')) {
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111, 99999).'.'.$extension;
                    $large_image_path = 'images/backend_images/products/large/' .$filename;
                    $medium_image_path = 'images/backend_images/products/medium/' .$filename;
                    $small_image_path = 'images/backend_images/products/small/' .$filename;

                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300, 300)->save($small_image_path);

                }
            } else {
                $filename = $data['current_image'];

            }

    		Product::where(['id' => $id])->update([
				'toko_id' => $data['toko_id'],
				'category_id' => $data['category_id'],
				'name' => $data['name'],
				'deskripsi' => $data['deskripsi'],
				'berat' => $data['berat'],
				'harga' => $data['harga'],
				'stok' => $data['stok'],
				'image' => $filename
               
    		]);
    		return redirect('/admin/product-view')->with('flash_message_success', 'Product edited successfully');
    	}
		$productDetail = Product::where(['id' => $id])->first();

    	return view('admin.products.edit_product')->with(compact('productDetail', 'levels'));
    }

    public function deleteProduct(Request $request, $id = null) {
    	if (!empty($id)) {
    		Product::where(['id' => $id])->delete();
    		return redirect()->back()->with('flash_message_success', 'Product was deleted successfully');
    	}
    }
}
