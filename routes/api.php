<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login','Api\UserController@login');
Route::post('/register','Api\UserController@register');
Route::post('/validasiUser','Api\UserController@validasiUser');
Route::post('/updateProfile','Api\UserController@updateProfile');
Route::get('/get_user_all','Api\UserController@getUser');

Route::post('/register_toko','Api\TokoController@registertoko');
Route::post('/updae_profile_toko','Api\TokoController@updateToko');
Route::get('/get_toko_all','Api\TokoController@getToko');
Route::post('/get_toko_user','Api\TokoController@getTokoUser');
Route::post('/add_product','Api\ProductController@addProduct');
Route::post('/delete_product','Api\ProductController@deleteProduct');
Route::post('/update_product','Api\ProductController@updateProduct');
Route::get('/get_product_all','Api\ProductController@getProduct');
Route::get('/get_category','Api\CategoryController@getCategory');

Route::post('/addOder','Api\OrderController@addOder');
Route::post('/updateOrder','Api\OrderController@updateOrder');
Route::get('/list_order','Api\OrderController@getOrder');
Route::post('/deleteOrder','Api\OrderController@deleteOrder');

Route::post('/addHeadChat','Api\HeadChatController@addHeadChat');
Route::get('/getHeadChat','Api\HeadChatController@getHeadChat');
Route::post('/addChat','Api\ChatController@addChat');
Route::get('/getChat','Api\ChatController@getChat');


Route::post('/addFavorite','Api\FavoriteController@addFavorite');
Route::get('/getFavorite','Api\FavoriteController@getFavorite');
Route::post('/deleteFavorite','Api\FavoriteController@deleteFavorite');

Route::post('/addInvestor','Api\InvestorController@addInvestor');
Route::get('/getInvestor','Api\InvestorController@getInvestor');
Route::post('/deleteInvestor','Api\InvestorController@deleteInvestor');

Route::post('/addUlasan','Api\UlasanController@addUlasan');
Route::get('/getUlasan','Api\UlasanController@getUlasan');
Route::post('/deleteUlasan','Api\UlasanController@deleteUlasan');
