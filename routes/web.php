<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Default Route ke web laravel
Route::get('/', function () {
    return view('welcome');
});



//Routing Admin
Route::match(['get' ,'post'], '/admin', 'AdminController@login');
Route::get('/admin/dashboard', 'AdminController@dashboard');
Route::get('/logout', 'AdminController@logout');

//Session dengan middleware
Route::group(['middleware'=> ['auth']], function() {
	Route::get('/admin/dashboard', 'AdminController@dashboard');
	Route::get('/admin/settings', 'AdminController@settings');

	//update password, setting di js matrix form-validate
	Route::get('/admin/check-pwd', 'AdminController@chkPassword');
	Route::match(['get' ,'post'], '/admin/update-pwd', 'AdminController@updatePassword');

	//User Routes (admin)
	Route::match(['get' ,'post'], '/admin/user-add', 'UserController@add');
	Route::get('/admin/user-view', 'UserController@getUser');
	Route::match(['get' ,'post'], '/admin/user-update/{id}', 'UserController@updateUser');
	Route::get('/admin/user-delete/{id}', 'UserController@deleteUser');

	//Category Routes (admin)
	Route::match(['get' ,'post'], '/admin/category-add', 'CategoryController@add');
	Route::get('/admin/category-view', 'CategoryController@getCategory');
	Route::match(['get' ,'post'], '/admin/category-update/{id}', 'CategoryController@updateCategory');
	Route::get('/admin/category-delete/{id}', 'CategoryController@deleteCategory');

	//Toko Routes (admin)
	Route::match(['get' ,'post'], '/admin/toko-add', 'TokoController@add');
	Route::get('/admin/toko-view', 'TokoController@getToko');
	Route::match(['get' ,'post'], '/admin/toko-update/{id}', 'TokoController@updateToko');
	Route::get('/admin/toko-delete/{id}', 'TokoController@deleteToko');

	//Product Routes (admin)
	Route::match(['get' ,'post'], '/admin/product-add', 'ProductController@add');
	Route::get('/admin/product-view', 'ProductController@getProduct');
	Route::match(['get' ,'post'], '/admin/product-update/{id}', 'ProductController@updateProduct');
	Route::get('/admin/product-delete/{id}', 'ProductController@deleteProduct');

	//Order Routes (admin)
	Route::match(['get' ,'post'], '/admin/order-add', 'OrderController@add');
	Route::get('/admin/order-view', 'OrderController@getOrder');
	Route::match(['get' ,'post'], '/admin/product-update/{id}', 'ProductController@updateProduct');
	Route::get('/admin/product-delete/{id}', 'ProductController@deleteProduct');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
