-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Apr 2019 pada 06.43
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kayuku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Living Room', NULL, '2019-04-04 04:28:28', '2019-04-04 04:28:28'),
(2, 'Dining Room', NULL, '2019-04-04 04:28:43', '2019-04-04 04:28:43'),
(3, 'Bedroom', NULL, '2019-04-04 04:29:03', '2019-04-04 04:29:03'),
(4, 'Workspace', NULL, '2019-04-04 04:29:30', '2019-04-04 04:29:30'),
(5, 'Kitchen', NULL, '2019-04-04 04:29:48', '2019-04-04 04:29:48'),
(6, 'Bathroom', NULL, '2019-04-04 04:30:06', '2019-04-04 04:30:06'),
(7, 'Decoration', NULL, '2019-04-04 04:30:20', '2019-04-04 04:30:20'),
(8, 'Collection', NULL, '2019-04-04 04:31:03', '2019-04-04 04:31:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `favorite`
--

CREATE TABLE `favorite` (
  `id` int(10) NOT NULL,
  `id_produk` int(10) NOT NULL,
  `customers_id` int(10) NOT NULL,
  `toko_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `image_primary` varchar(100) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `favorite`
--

INSERT INTO `favorite` (`id`, `id_produk`, `customers_id`, `toko_id`, `name`, `deskripsi`, `harga`, `image_primary`, `updated_at`, `created_at`) VALUES
(2, 1, 2, 3, '4', '4', '5', '1', '2019-04-07 13:32:53', '2019-04-07 13:32:53'),
(8, 3, 11, 5, 'Testing', 'Testing', '1', 'imageproduk_Testing', '2019-04-07 15:32:30', '2019-04-07 15:32:30'),
(9, 15, 11, 9, 'Nana', 'Oh nana', '6', 'imageproduk_Nana', '2019-04-07 15:52:34', '2019-04-07 15:52:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `investor`
--

CREATE TABLE `investor` (
  `id` int(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tlp` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pinjaman` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `investor`
--

INSERT INTO `investor` (`id`, `nama`, `deskripsi`, `alamat`, `tlp`, `email`, `pinjaman`, `image`, `updated_at`, `created_at`) VALUES
(1, 'dfdsfdfdsf', 'dff', 'dfd', 'sdfsdf', 'fdsf', 'dsfsdf', '89e5dee27f1728e1d52527b702628e80.jpg', '2019-04-07 20:20:21', '2019-04-07 20:20:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konfirmasi`
--

CREATE TABLE `konfirmasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_31_111151_create_category_table', 1),
(4, '2019_04_02_013828_create_product_table', 1),
(5, '2019_04_02_013916_create_toko_table', 1),
(6, '2019_04_02_013952_create_order_table', 1),
(7, '2019_04_02_014046_create_order_detail_table', 1),
(8, '2019_04_02_015706_create_konfirmasi_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `order`
--

CREATE TABLE `order` (
  `id` int(10) NOT NULL,
  `customers_id` int(100) NOT NULL,
  `toko_id` int(100) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `qty` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kuantitas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_list`
--

CREATE TABLE `order_list` (
  `id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `customers_name` varchar(100) NOT NULL,
  `toko_id` int(11) NOT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `status_image` varchar(100) DEFAULT NULL,
  `qty` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `order_list`
--

INSERT INTO `order_list` (`id`, `customers_id`, `customers_name`, `toko_id`, `kode`, `name`, `image`, `harga`, `status`, `status_image`, `qty`, `total`, `alamat`, `created_at`, `updated_at`) VALUES
(9, 11, '', 5, 'KAYUKU-09-005', 'Testing', 'imageproduk_Testing', '1', 'cancel', NULL, '3', '2', 'Yu', '2019-04-07 07:55:00', '2019-04-07 00:55:00'),
(10, 11, '', 5, 'KAYUKU-010-005', 'Saya', 'imageproduk_Saya', '7854', 'order', NULL, '1', '7854', 'Bb', '2019-04-07 08:08:45', '2019-04-07 01:08:45'),
(11, 11, '', 5, NULL, 'Testing', 'imageproduk_Testing', '1', 'troli', NULL, NULL, NULL, NULL, '2019-04-07 01:16:59', '2019-04-07 01:16:59'),
(12, 15, '', 5, 'KAYUKU-012-005', 'Testing', 'imageproduk_Testing', '1', 'order', NULL, '1', '1', 'Jsjs', '2019-04-07 15:46:11', '2019-04-07 08:46:11'),
(13, 11, 'Muhamat Zaenal Mahmut', 9, 'KAYUKU-013-009', 'Nana', 'imageproduk_Nana', '6', 'order', NULL, '1', '6', 'Nznz', '2019-04-07 16:00:33', '2019-04-07 09:00:33'),
(14, 11, 'Muhamat Zaenal Mahmut', 5, NULL, 'Testing', 'imageproduk_Testing', '1', 'troli', NULL, NULL, NULL, NULL, '2019-04-07 15:49:17', '2019-04-07 15:49:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `toko_id` int(11) NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_primary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_secondary` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id`, `toko_id`, `category_id`, `name`, `deskripsi`, `berat`, `harga`, `stok`, `image_primary`, `image_secondary`, `created_at`, `updated_at`) VALUES
(1, 5, '1', 'Lemari Dapur Rak Susun Coco Helm Series', 'Lemari dapur dengan nuansa vintage menyuguhkan suasana kenyamanan yang elegan di ruang rumah Anda. Dekorasi yang unit dan natural akan membuat ruangan Anda semakin istimewa', '30', '3450000', '30', '59462.png', NULL, '2019-04-04 05:06:13', '2019-04-04 05:06:13'),
(3, 5, '7', 'Testing', 'Testing', '1006', '1', '55', 'imageproduk_Testing', NULL, '2019-04-05 16:33:53', '2019-04-05 23:10:09'),
(13, 5, '2', 'Saya', 'Znnz', '97', '7854', '1', 'imageproduk_Saya', NULL, '2019-04-05 20:08:36', '2019-04-05 20:08:36'),
(15, 9, '4', 'Nana', 'Oh nana', '9', '6', '1', 'imageproduk_Nana', NULL, '2019-04-06 02:21:10', '2019-04-06 02:21:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `toko`
--

CREATE TABLE `toko` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `informasi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `toko`
--

INSERT INTO `toko` (`id`, `user_id`, `name`, `alamat`, `telp`, `informasi`, `image`, `created_at`, `updated_at`) VALUES
(5, '11', 'zencode', 'Surabaya', 'dfds', 'dsfdsf', '500_F_206500217_Z4JwQt9gCJRIQHadJCJTiHo8QdDKYlXx.jpg', '2019-04-05 05:21:30', '2019-04-05 16:14:58'),
(6, '12', 'Nj', NULL, NULL, NULL, NULL, '2019-04-06 00:55:57', '2019-04-06 00:55:57'),
(7, '13', 'Iu', NULL, NULL, NULL, NULL, '2019-04-06 00:57:45', '2019-04-06 00:57:45'),
(8, '14', 'Hjk', NULL, NULL, NULL, NULL, '2019-04-06 01:03:29', '2019-04-06 01:03:29'),
(9, '15', 'admin', 'Admin', '082', 'Admin', 'imagetoko_admin', '2019-04-06 02:19:29', '2019-04-06 02:26:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ulasan`
--

CREATE TABLE `ulasan` (
  `id` int(10) NOT NULL,
  `produk_id` int(10) DEFAULT NULL,
  `customers_id` int(10) NOT NULL,
  `customers_name` int(10) NOT NULL,
  `image` varchar(100) NOT NULL,
  `toko_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `ulasan` varchar(100) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ulasan`
--

INSERT INTO `ulasan` (`id`, `produk_id`, `customers_id`, `customers_name`, `image`, `toko_id`, `date`, `ulasan`, `updated_at`, `created_at`) VALUES
(2, 1, 1, 1, '', 5, '2019-08-01', '1', '2019-04-07 16:52:33', '2019-04-07 16:52:33'),
(3, 1, 1, 1, '', 5, '2019-08-01', '1', '2019-04-07 17:07:11', '2019-04-07 17:07:11'),
(4, 1, 1, 1, '89e5dee27f1728e1d52527b702628e80.jpg', 5, '2019-08-01', 'dfdsfdsfsdfdsffsdfdsfdsfdsfsdfsffssfsdfddddddddddddddddddddddd', '2019-04-07 17:45:14', '2019-04-07 17:45:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `jenis_kelamin`, `username`, `email`, `email_verified_at`, `password`, `admin`, `address`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', '', 'admin@gmail.com', NULL, '$2y$10$e2gQj8l8FqE/d6DFzf1zzuZFn6rs7Z6QkrbwYYMjnYHz7/HD8ipR6', 1, '', '', '2ZCUkYV4tpBImJ1IlN8Eyy3vYdcrNgvXJnTQJXciwzTBnul3vv7HtIHGSN4h', NULL, NULL),
(11, 'Muhamat Zaenal Mahmut', 'Laki-Laki', 'zencode', 'zen@gmail.com', NULL, '32b22fb599ea09dfed3b7acb7ef1287c94f8301d', NULL, 'Surabaya', 'imageuser_Muhamat Zaenal Mahmut', NULL, '2019-04-05 05:21:29', '2019-04-07 16:29:07'),
(12, 'Dj', 'Laki-Laki', 'Nj', 'B', NULL, '80c3d7b3f509a5ac8bb29d7f8c4a94af14f7d244', NULL, NULL, NULL, NULL, '2019-04-06 00:55:56', '2019-04-06 00:55:56'),
(13, 'Yubb', 'Perempuan', 'Iu', 'Vhh', NULL, '5a258230180d9c643f761089d7e33b8b52288ed3', NULL, NULL, NULL, NULL, '2019-04-06 00:57:44', '2019-04-06 00:57:44'),
(14, 'Njj', 'Laki-Laki', 'Hjk', 'Jjk', NULL, 'b821103a5881b0d768dd9f697df6b0b7951110b2', NULL, NULL, NULL, NULL, '2019-04-06 01:03:28', '2019-04-06 01:03:28'),
(15, 'Admin', 'Laki-Laki', 'admin', 'Admin', NULL, 'd033e22ae348aeb5660fc2140aec35850c4da997', NULL, NULL, NULL, NULL, '2019-04-06 02:19:28', '2019-04-06 02:19:28');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `investor`
--
ALTER TABLE `investor`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `konfirmasi`
--
ALTER TABLE `konfirmasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `toko`
--
ALTER TABLE `toko`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ulasan`
--
ALTER TABLE `ulasan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `investor`
--
ALTER TABLE `investor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `konfirmasi`
--
ALTER TABLE `konfirmasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `toko`
--
ALTER TABLE `toko`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `ulasan`
--
ALTER TABLE `ulasan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
